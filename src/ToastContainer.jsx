var React = require('react');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');
var ToastItem = require('./ToastItem');
var Constants = require('./constants');

var ToastContainer = createReactClass({

  propTypes: {
    position: PropTypes.string.isRequired,
    toasts: PropTypes.array.isRequired,
    getStyles: PropTypes.object
  },

  _style: {},

  notificationToMount: function() {
    // Fix position if width is overriden (I don't know a single display to have 320 pixels)
    this._style = this.props.getStyles.container(this.props.position);

    if (this.props.getStyles.overrideWidth && (this.props.position === Constants.positions.tc || this.props.position === Constants.positions.bc)) {
      this._style.marginLeft = -(this.props.getStyles.overrideWidth / 2);
    }
  },

  render: function() {
    var self = this;
    var toasts;

    if ([Constants.positions.bl, Constants.positions.br, Constants.positions.bc].indexOf(this.props.position) > -1) {
      this.props.toasts.reverse();
    }

    toasts = this.props.toasts.map(function(toast) {
      return (
        <ToastItem
          ref={ 'toast-' + toast.uid }
          key={ toast.uid }
          toast={ toast }
          getStyles={ self.props.getStyles }
          onRemove={ self.props.onRemove }
          noAnimation={ self.props.noAnimation }
          allowHTML={ self.props.allowHTML }
          children={ self.props.children }
        />
      );
    });

    return (
      <div className={ 'toasts-' + this.props.position } style={ this._style }>
        { toasts }
      </div>
    );
  }
});


module.exports = ToastContainer;
