var React = require('react');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');
var ReactDOM = require('react-dom');
var Constants = require('./constants');
var Helpers = require('./helpers');
var merge = require('object-assign');

/* From Modernizr */
var whichTransitionEvent = function() {
  var el = document.createElement('fakeelement');
  var transition;
  var transitions = {
    transition: 'transitionend',
    OTransition: 'oTransitionEnd',
    MozTransition: 'transitionend',
    WebkitTransition: 'webkitTransitionEnd'
  };

  Object.keys(transitions).forEach(function(transitionKey) {
    if (el.style[transitionKey] !== undefined) {
      transition = transitions[transitionKey];
    }
  });

  return transition;
};

var ToastItem = createReactClass({
  propTypes: {
    toast: PropTypes.object,
    getStyles: PropTypes.object,
    onRemove: PropTypes.func,
    allowHTML: PropTypes.bool,
    noAnimation: PropTypes.bool,
    children: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element
    ])
  },

  getDefaultProps: function() {
    return {
      noAnimation: false,
      onRemove: function() {},
      allowHTML: false
    };
  },

  getInitialState: function() {
    return {
      visible: undefined,
      removed: false
    };
  },

  notificationToMount: function() {
    var getStyles = this.props.getStyles;
    var level = this.props.toast.level;
    var dismissible = this.props.toast.dismissible;

    this._noAnimation = this.props.noAnimation;

    this._styles = {
      toast: getStyles.byElement('toast')(level),
      title: getStyles.byElement('title')(level),
      dismiss: getStyles.byElement('dismiss')(level),
      messageWrapper: getStyles.byElement('messageWrapper')(level),
      actionWrapper: getStyles.byElement('actionWrapper')(level),
      action: getStyles.byElement('action')(level)
    };

    if (!dismissible || dismissible === 'none' || dismissible === 'button') {
      this._styles.toast.cursor = 'default';
    }
  },

  _styles: {},

  _toastTimer: null,

  _height: 0,

  _noAnimation: null,

  _isMounted: false,

  _removeCount: 0,

  _getCssPropertyByPosition: function() {
    var position = this.props.toast.position;
    var css = {};

    switch (position) {
    case Constants.positions.tl:
    case Constants.positions.bl:
      css = {
        property: 'left',
        value: -200
      };
      break;

    case Constants.positions.tr:
    case Constants.positions.br:
      css = {
        property: 'right',
        value: -200
      };
      break;

    case Constants.positions.tc:
      css = {
        property: 'top',
        value: -100
      };
      break;

    case Constants.positions.bc:
      css = {
        property: 'bottom',
        value: -100
      };
      break;

    default:
    }

    return css;
  },

  _defaultAction: function(event) {
    var toast = this.props.toast;

    event.preventDefault();
    this._hidetoast();
    if (typeof toast.action.callback === 'function') {
      toast.action.callback();
    }
  },

  _hidetoast: function() {
    if (this._toastTimer) {
      this._toastTimer.clear();
    }

    if (this._isMounted) {
      this.setState({
        visible: false,
        removed: true
      });
    }

    if (this._noAnimation) {
      this._removetoast();
    }
  },

  _removetoast: function() {
    this.props.onRemove(this.props.toast.uid);
  },

  _dismiss: function() {
    if (!this.props.toast.dismissible) {
      return;
    }

    this._hidetoast();
  },

  _showtoast: function() {
    var self = this;
    setTimeout(function() {
      if (self._isMounted) {
        self.setState({
          visible: true
        });
      }
    }, 50);
  },

  _onTransitionEnd: function() {
    if (this._removeCount > 0) return;
    if (this.state.removed) {
      this._removeCount += 1;
      this._removetoast();
    }
  },

  notificationDidMount: function() {
    var self = this;
    var transitionEvent = whichTransitionEvent();
    var toast = this.props.toast;
    var element = ReactDOM.findDOMNode(this);

    this._height = element.offsetHeight;

    this._isMounted = true;

    // Watch for transition end
    if (!this._noAnimation) {
      if (transitionEvent) {
        element.addEventListener(transitionEvent, this._onTransitionEnd);
      } else {
        this._noAnimation = true;
      }
    }


    if (toast.autoDismiss) {
      this._toastTimer = new Helpers.Timer(function() {
        self._hidetoast();
      }, toast.autoDismiss * 1000);
    }

    this._showtoast();
  },

  _handleMouseEnter: function() {
    var toast = this.props.toast;
    if (toast.autoDismiss) {
      this._toastTimer.pause();
    }
  },

  _handleMouseLeave: function() {
    var toast = this.props.toast;
    if (toast.autoDismiss) {
      this._toastTimer.resume();
    }
  },

  _handletoastClick: function() {
    var dismissible = this.props.toast.dismissible;
    if (dismissible === 'both' || dismissible === 'click' || dismissible === true) {
      this._dismiss();
    }
  },

  notificationWillUnmount: function() {
    var element = ReactDOM.findDOMNode(this);
    var transitionEvent = whichTransitionEvent();
    element.removeEventListener(transitionEvent, this._onTransitionEnd);
    this._isMounted = false;
  },

  _allowHTML: function(string) {
    return { __html: string };
  },

  render: function() {
    var toast = this.props.toast;
    var className = 'toast toast-' + toast.level;
    var toastStyle = merge({}, this._styles.toast);
    var cssByPos = this._getCssPropertyByPosition();
    var dismiss = null;
    var actionButton = null;
    var title = null;
    var message = null;

    if (this.state.visible) {
      className += ' toast-visible';
    } else if (this.state.visible === false) {
      className += ' toast-hidden';
    }

    if (toast.dismissible === 'none') {
      className += ' toast-not-dismissible';
    }

    if (this.props.getStyles.overrideStyle) {
      if (!this.state.visible && !this.state.removed) {
        toastStyle[cssByPos.property] = cssByPos.value;
      }

      if (this.state.visible && !this.state.removed) {
        toastStyle.height = this._height;
        toastStyle[cssByPos.property] = 0;
      }

      if (this.state.removed) {
        toastStyle.overlay = 'hidden';
        toastStyle.height = 0;
        toastStyle.marginTop = 0;
        toastStyle.paddingTop = 0;
        toastStyle.paddingBottom = 0;
      }
      toastStyle.opacity = this.state.visible ? this._styles.toast.isVisible.opacity : this._styles.toast.isHidden.opacity;
    }

    if (toast.title) {
      title = <h4 className="toast-title" style={ this._styles.title }>{ toast.title }</h4>;
    }

    if (toast.message) {
      if (this.props.allowHTML) {
        message = (
          <div className="toast-message" style={ this._styles.messageWrapper } dangerouslySetInnerHTML={ this._allowHTML(toast.message) } />
        );
      } else {
        message = (
          <div className="toast-message" style={ this._styles.messageWrapper }>{ toast.message }</div>
        );
      }
    }
    if (toast.dismissible === 'both' || toast.dismissible === 'button' || toast.dismissible === true) {
      dismiss = <span className="toast-dismiss" onClick={ this._dismiss } style={ this._styles.dismiss }>&times;</span>;
    }

    if (toast.action) {
      actionButton = (
        <div className="toast-action-wrapper" style={ this._styles.actionWrapper }>
          <button className="toast-action-button"
            onClick={ this._defaultAction }
            style={ this._styles.action }>
            { toast.action.label }
          </button>
        </div>
      );
    }

    if (toast.children) {
      actionButton = toast.children;
    }

    return (
      <div className={ className } onClick={ this._handletoastClick } onMouseEnter={ this._handleMouseEnter } onMouseLeave={ this._handleMouseLeave } style={ toastStyle }>
        { title }
        { message }
        { dismiss }
        { actionButton }
      </div>
    );
  }

});

module.exports = ToastItem;
