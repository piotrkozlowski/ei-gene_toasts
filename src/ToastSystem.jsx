var React = require('react');
var createReactClass = require('create-react-class');
var PropTypes = require('prop-types');
var merge = require('object-assign');
var ToastContainer = require('./ToastContainer');
var Constants = require('./constants');
var Styles = require('./styles');

var toastSystem = createReactClass({

  uid: 3400,

  _isMounted: false,

  _getStyles: {
    overrideStyle: {},

    overrideWidth: null,

    setOverrideStyle: function(style) {
      this.overrideStyle = style;
    },

    wrapper: function() {
      if (!this.overrideStyle) return {};
      return merge({}, Styles.Wrapper, this.overrideStyle.Wrapper);
    },

    container: function(position) {
      var override = this.overrideStyle.Containers || {};
      if (!this.overrideStyle) return {};

      this.overrideWidth = Styles.Containers.DefaultStyle.width;

      if (override.DefaultStyle && override.DefaultStyle.width) {
        this.overrideWidth = override.DefaultStyle.width;
      }

      if (override[position] && override[position].width) {
        this.overrideWidth = override[position].width;
      }

      return merge({}, Styles.Containers.DefaultStyle, Styles.Containers[position], override.DefaultStyle, override[position]);
    },

    elements: {
      toast: 'ToastItem',
      title: 'Title',
      messageWrapper: 'MessageWrapper',
      dismiss: 'Dismiss',
      action: 'Action',
      actionWrapper: 'ActionWrapper'
    },

    byElement: function(element) {
      var self = this;
      return function(level) {
        var _element = self.elements[element];
        var override = self.overrideStyle[_element] || {};
        if (!self.overrideStyle) return {};
        return merge({}, Styles[_element].DefaultStyle, Styles[_element][level], override.DefaultStyle, override[level]);
      };
    }
  },

  _notificationRemoved: function(uid) {
    var toast;
    var toasts = this.state.toasts.filter(function(toCheck) {
      if (toCheck.uid === uid) {
        toast = toCheck;
        return false;
      }
      return true;
    });

    if (this._isMounted) {
      this.setState({ toasts: toasts });
    }

    if (toast && toast.onRemove) {
      toast.onRemove(toast);
    }
  },

  getInitialState: function() {
    return {
      toasts: []
    };
  },

  propTypes: {
    style: PropTypes.oneOfType([
      PropTypes.bool,
      PropTypes.object
    ]),
    noAnimation: PropTypes.bool,
    allowHTML: PropTypes.bool
  },

  getDefaultProps: function() {
    return {
      style: {},
      noAnimation: false,
      allowHTML: false
    };
  },

  addtoast: function(toast) {
    var _toast = merge({}, Constants.toast, toast);
    var toasts = this.state.toasts;
    var i;

    if (!_toast.level) {
      throw new Error('toast level is required.');
    }

    if (Object.keys(Constants.levels).indexOf(_toast.level) === -1) {
      throw new Error('\'' + _toast.level + '\' is not a valid level.');
    }

    // eslint-disable-next-line
    if (isNaN(_toast.autoDismiss)) {
      throw new Error('\'autoDismiss\' must be a number.');
    }

    if (Object.keys(Constants.positions).indexOf(_toast.position) === -1) {
      throw new Error('\'' + _toast.position + '\' is not a valid position.');
    }

    _toast.position = _toast.position.toLowerCase();
    _toast.level = _toast.level.toLowerCase();
    _toast.autoDismiss = parseInt(_toast.autoDismiss, 10);

    _toast.uid = _toast.uid || this.uid;
    _toast.ref = 'toast-' + _toast.uid;
    this.uid += 1;

    // do not add if the toast already exists based on supplied uid
    for (i = 0; i < toasts.length; i += 1) {
      if (toasts[i].uid === _toast.uid) {
        return false;
      }
    }

    toasts.push(_toast);

    if (typeof _toast.onAdd === 'function') {
      toast.onAdd(_toast);
    }

    this.setState({
      toasts: toasts
    });

    return _toast;
  },

  gettoastRef: function(toast) {
    var self = this;
    var foundtoast = null;

    Object.keys(this.refs).forEach(function(container) {
      if (container.indexOf('container') > -1) {
        Object.keys(self.refs[container].refs).forEach(function(_toast) {
          var uid = toast.uid ? toast.uid : toast;
          if (_toast === 'toast-' + uid) {
            // NOTE: Stop iterating further and return the found toast.
            // Since UIDs are uniques and there won't be another toast found.
            foundtoast = self.refs[container].refs[_toast];
          }
        });
      }
    });

    return foundtoast;
  },

  removetoast: function(toast) {
    var foundtoast = this.gettoastRef(toast);
    return foundtoast && foundtoast._hidetoast();
  },

  edittoast: function(toast, newtoast) {
    var foundtoast = null;
    // NOTE: Find state toast to update by using
    // `setState` and forcing React to re-render the component.
    var uid = toast.uid ? toast.uid : toast;

    var newtoasts = this.state.toasts.filter(function(statetoast) {
      if (uid === statetoast.uid) {
        foundtoast = statetoast;
        return false;
      }

      return true;
    });


    if (!foundtoast) {
      return;
    }

    newtoasts.push(merge(
      {},
      foundtoast,
      newtoast
    ));

    this.setState({
      toasts: newtoasts
    });
  },

  cleartoasts: function() {
    var self = this;
    Object.keys(this.refs).forEach(function(container) {
      if (container.indexOf('container') > -1) {
        Object.keys(self.refs[container].refs).forEach(function(_toast) {
          self.refs[container].refs[_toast]._hidetoast();
        });
      }
    });
  },

  notificationDidMount: function() {
    this._getStyles.setOverrideStyle(this.props.style);
    this._isMounted = true;
  },

  notificationWillUnmount: function() {
    this._isMounted = false;
  },

  render: function() {
    var self = this;
    var containers = null;
    var toasts = this.state.toasts;

    if (toasts.length) {
      containers = Object.keys(Constants.positions).map(function(position) {
        var _toasts = toasts.filter(function(toast) {
          return position === toast.position;
        });

        if (!_toasts.length) {
          return null;
        }

        return (
          <ToastContainer
            ref={ 'container-' + position }
            key={ position }
            position={ position }
            toasts={ _toasts }
            getStyles={ self._getStyles }
            onRemove={ self._notificationRemoved }
            noAnimation={ self.props.noAnimation }
            allowHTML={ self.props.allowHTML }
          />
        );
      });
    }


    return (
      <div className="toasts-wrapper" style={ this._getStyles.wrapper() }>
        { containers }
      </div>
    );
  }
});

module.exports = toastSystem;
